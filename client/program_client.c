#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netdb.h>
#include <netinet/in.h>
#include <string.h>
#include <pthread.h>
#include <errno.h>
#include <stdlib.h>

#define HOST "127.0.0.1"
#define PORT 8080

void *inServer(void *client_fd)
{
	char query[1000] = {0};
	int fd = *(int *)client_fd;

	printf("Welcome to database F09 🚀\n");

	while (1)
	{
		printf("F09> ");

		fgets(query, 1000, stdin);
		char *temp = strtok(query, "\n");

		if (temp != NULL){
			strcpy(query, temp);
		}

		if (strcmp(query, "exit") == 0){
			exit(EXIT_SUCCESS);
		}

		send(fd, query, sizeof(char) * 1000, 0);
	}
}

void *outServer(void *client_fd)
{
	char query[1000] = {0};
	int fd = *(int *)client_fd;

	while (1)
	{
		memset(query, 0, sizeof(char) * 1000);

		if (recv(fd, query, 1000, 0) == 0){
			exit(EXIT_SUCCESS);
		}

		printf("%s", query);
		fflush(stdout);
	}
}

int main(int argc, char *argv[])
{
	pthread_t threadID[2];

	int client_fd, opt = 1;
	struct sockaddr_in address;
	struct hostent *host;

	address.sin_family = AF_INET;
	address.sin_port = htons(PORT);
	host = gethostbyname(HOST);
	address.sin_addr = *((struct in_addr *)host->h_addr);

	if ((client_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0){
		perror("socket failed"); exit(EXIT_FAILURE);
	}

	if (setsockopt(client_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))){
		perror("setsockopt"); exit(EXIT_FAILURE);
	}

	if (connect(client_fd, (struct sockaddr *)&address, sizeof(struct sockaddr_in)) == -1){
		exit(EXIT_FAILURE);
	}

	pthread_create(&(threadID[0]), NULL, &inServer, (void *)&client_fd);
	pthread_create(&(threadID[1]), NULL, &outServer, (void *)&client_fd);

	pthread_join(threadID[0], NULL);
	pthread_join(threadID[1], NULL);

	close(client_fd);

	return 0;
}
